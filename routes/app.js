var express = require('express');
var router = express.Router();
var Post = require('../models/post');

function init(){
  Post.findOne({_id: 0}, function(err, doc){
    if(doc){
      return true;
    }
    else{
      var postZero = new Post({
        content: "Post 0",
        parent: -1,
        children: []
      })
      postZero.save();
    }
  })
}

router.get('/', function(req, res, next){
  init();
  res.render('index');
});

router.get('/posts/:postId', function (req, res, next){
  Post.findOne({_id: req.params.postId}, function(err, post){
    if(err){
      res.send('Error');
    }
    if(post){
      res.json(post);
    }
  });
});

function bump(post){
  Post.findOne({_id: post.parent}, function(err, parent){
    if(err){
      return res.send('Error');
    }
    if(parent){
      if(parent.children.indexOf(post._id) !== -1){
        doc.children.splice(index, 1);
      }
      parent.children.unshift(post._id);
      parent.save();
      bump(parent);
    }
  })
}

router.post('/', function (req, res, next) {
  var content = req.body.content;
  var parent = req.body.parent || 0;
  var post = new Post({
    content: content,
    parent: parent,
    children: []
  });
  post.save().then(function(){
    bump(post);
    res.redirect('/');
  });
});

module.exports = router;
