var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');


var connection = mongoose.createConnection("mongodb://localhost/401chan");
autoIncrement.initialize(connection);

var schema = new Schema({
    content: {type: String, required: true},
    parent: {type: Number, ref: 'Post'},
    children: [{type: Number, ref: 'Post'}],
    timestamp: {type: Date, default: Date.now}
});

schema.plugin(autoIncrement.plugin, {model: 'Post'});
module.exports = connection.model('Post', schema);
